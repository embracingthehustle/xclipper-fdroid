package com.kpstv.xclipper.ui.helpers

import android.app.Activity.RESULT_OK
import android.content.Context
import android.content.Intent
import androidx.activity.ComponentActivity
import androidx.activity.result.contract.ActivityResultContract
import com.kpstv.xclipper.data.localized.FBOptions
import com.kpstv.xclipper.data.provider.DBConnectionProvider
import com.kpstv.xclipper.extensions.listeners.ResponseListener

/**
 * Helper class to make Firebase auth process simpler
 * @param activity [ComponentActivity] to listen [ActivityResultContract]
 * @param clientId An authentication clientId provided by [FBOptions]
 */
class AuthenticationHelper(
    private val activity: ComponentActivity,
    private var clientId: String
) {
    private lateinit var responseListener: ResponseListener<Unit>

    fun signIn(options: FBOptions, responseListener: ResponseListener<Unit>) = with(activity) {
    }

}